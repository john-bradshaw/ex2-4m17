/**
 * 
 */
package exercise2;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;


/**
 * @author eng-188do
 * This class holds route and runs the simulated annealing code
 */
public class SimAnneal {
	//Constructor
	public SimAnneal(long seed){
		SimAnnealBasicConstruct(seed);
		logging=false;
	}
	
	public SimAnneal(long seed, LogClass loggerIn){
		logging=true;
		logger=loggerIn;
		SimAnnealBasicConstruct(seed);
	}
	
	private void SimAnnealBasicConstruct(long seed){
		points=new Route();
		rnd = new Random(seed);
		iter=0;
	}
	
	//Attributes
	private Route points;
	private Random rnd; 
	private final Boolean logging;
	private LogClass logger;
	private long iter; //TODO: maybe check that this doesn't overflow?
	
	//Getters/Setters	
	public void setPoints(Route pointsIn){
		this.points=pointsIn;
	}
	
	public Route getPoints() {
		return points;
	}
	
	
	//Public Members
	/**
	 * Sets up all the points in memory.
	 */
	public void initialisePoints(String location){
		points.clear();
		String home=System.getProperty("user.home");
		readInCsv(home + location); //This is where I store the file
	}
	
	/**
	 * Gets the distance of the current route.
	 * @return euclidean distance
	 */
	public double getDistance(){
		return points.getDistance();
	}
	
	/**
	 * 	  @return list of names of points in route in order
	 */
	public List<String> getRoute(){
		List<String> routeNames = new ArrayList();
		List<CoordPt> routePoints= points.getRoutePts();
		
		for(int i=0; i<points.getNumPts(); i++){
			routeNames.add(routePoints.get(i).getName());
		}
		return routeNames;
	}
	
	/**
	 * Runs simulated Annealing process.
	 */
	public void runSimAnneal(){
		
		double temp=initialTemp(40);
		int maxNumConfig=100*points.getNumPts(); //maximum number of configurations
		int maxNumSuccessConfig=50*points.getNumPts(); //maximum number of successful configurations
		System.out.println("Initial temp is " + Double.toString(temp));
		
		while (temp > 1e-10) {
			runOneIterSimAnneal(maxNumConfig, maxNumSuccessConfig, temp);
			temp*=0.9;
		}
		
	}
	
	
	//Private members
	/**
	 * Runs one iteration of simulated annealing.
	 * By iteration I mean one stage of constant temp.
	 * @param maxNumConfig maximum number of configurations to try
	 * @param maxNumSuccessConfig maximum number of successful configurations to try (ie ones where we have moved)
	 * @param temp temperature
	 */
	private void runOneIterSimAnneal(int maxNumConfig, int maxNumSuccessConfig,double temp) {
		int numConfig=0;
		int numSuccessConfig=0;		
		double currentE=points.getDistance();
		
		while (numConfig <= maxNumConfig && numSuccessConfig <= maxNumSuccessConfig){		
			Route tempList= new Route(points);	
			
			/* Switching Algorithms uncomment out which to use */
			/*if (rnd.nextBoolean()){ //two options for choosing new options
				runSplice(tempList); //cut section and paste
			} else{
				runReverse(tempList); //reverse section
			}*/
			//runSwitch(tempList); //swap two points
			runNeighbourSwitch(tempList); /swap two neighboring points
			/* End of Switching Algorithms */
			
			double tempE=tempList.getDistance();
			double prob=calcProb( tempE, currentE,temp);
			if (rnd.nextDouble() < prob) {
				points=tempList;
				currentE=points.getDistance();
				numSuccessConfig++; // we have moved, albeit this could have been uphill.
			}
			if (logging && temp>1) //log only when temp is greater than 1
				performLogging(iter,currentE,temp);
			numConfig++;	
			iter++;
		}
	}
	
	/**
	 * Performs the necessary logging
	 * @param currentE current energy
	 * @param temp current temperature
	 * @param temp2 
	 */
	private void performLogging(long iter, double currentE, double temp) {
		logger.addPoint(currentE,temp, iter);		
	}

	/**
	 * Runs code where we reverse section of string
	 * @param tempList temporary list of the route.
	 */
	private void runReverse(Route tempList) {
		int[] range = generateTwoDistinctInts(tempList.getNumPts());
		tempList.switchPts(range[0], range[1]);		
	}
	
	/**
	 * Runs code where we reverse two successive points
	 * @param tempList temporary list of the route.
	 */
	private void runNeighbourSwitch(Route tempList) {
		int numPts=tempList.getNumPts();
		int index = rnd.nextInt(numPts-1)+1;
		if (index < numPts -2)
			tempList.switchPts(index, index+2);
		else
			tempList.switchPts(index-2, index);
	}
	
	
	/**
	 * Runs code where we reverse two no successive points points
	 * @param tempList temporary list of the route.
	 */
	private void runSwitch(Route tempList) {
		int[] range = generateTwoDistinctInts(tempList.getNumPts());
		tempList.swapPts(range[0], range[1]);		
	}
	
	/**
	 * Runs code where we remove section of string and paste it elsewehre
	 * @param tempList temporary list of the route.
	 */
	private void runSplice(Route tempList){
		int[] range = generateTwoDistinctInts(tempList.getNumPts());
		int numLeft=tempList.getNumPts()-range[1]+range[0];
		
		int insertionPosition;
		if (numLeft==1)
				insertionPosition=1;
		else
			insertionPosition=rnd.nextInt(numLeft-1)+1; //+1 as don't want it to go before 0.
		
		
		tempList.removeAndSplice(range[0], range[1], insertionPosition);
		//TODO: check this allows all possible ones - be careful with indices
	}
	
	
	/** 
	 * Generates two random integers in range of number of points (excluding 0)
	 * @param numPts number of points in route
	 * @return two integers. they are ordered, cannot be the same or zero.
	 */
	private int[] generateTwoDistinctInts(int numPts) {
		//Generate two random numbers between start and end of route
		int[] results = new int[2];
		results[0]=rnd.nextInt(numPts-1)+1; //=1 as don't want it to include 0
		results[1]=rnd.nextInt(numPts-1)+1;
		
		if (numPts <1)
			throw new IllegalArgumentException("Route is too small - solve by hand!");
		
		while (results[0]==results[1])
			results[1]=rnd.nextInt(numPts-1)+1; //get distinct results
		
		Arrays.sort( results );	
				
		return results;
	}

	/**
	 * Reads in csv of specified file format ie
	 * $name, $xCoord, $yCoord
	 * @param name where file is located (full path)
	 */
	private void readInCsv(String name){
		BufferedReader br = null;
		String line = ""; //temp variable for holding lines
		String splitter = ","; //what splits our files up
	 
		try {	 
			br = new BufferedReader(new FileReader(name));
			int i=0;
			while ((line = br.readLine()) != null) {
				String[] lineData = line.split(splitter);	//Split up inputs 
				points.addPt(Double.parseDouble(lineData[1]), Double.parseDouble(lineData[2]),
						 lineData[0],i);	 
			}	 
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally { //try closing the file.
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	 
		System.out.println("Read in " + name);
	}
	
	/**
	 * Calulates the probability of moving
	 * @param length1 new potential route's length
	 * @param length2 old potential route's length
	 * @param temp temperature
	 * @return probability (0.0-1.0)
	 */
	private double calcProb(double length1, double length2, double temp) {
		double prob=Math.exp(-(length1-length2)/temp);
		return (prob>1.0)? 1.0 : prob;
	}
	
	/**
	 * Calculate the initial temperature and randomly initialise the route.
	 * it calculates the initial temperature to use by looking at the kind of differences you are likely
	 * to encounter. It will also store the best found route in this process as a starting point.
	 * @param trials number of random initialisations to try
	 * @return temperature
	 */
	private double initialTemp(int trials) {
		double highestE=points.getDistance();
		double lowestE=highestE;
		double tempE; //temprary storage
		int FACTOR=10; //multiply factor at end.
		Route tempList= new Route(points);
		
		//now we look at several other points and see store the highest and lowest energies (ie distances) found
		for(int i=0; i<trials; i++) { 
			tempList.randomiseFull(rnd);
			tempE=tempList.getDistance();
			if (tempE<lowestE){				
				lowestE=tempE;
			} else if(tempE>highestE){
				highestE=tempE;
			}			
		}
		points=tempList; // we will also randomly initialise the points to give different starting points
		return FACTOR*(highestE-lowestE);			
	}



}	


