%% Script to see how adding random gaussian noise onto coord inputs affects outputs
%set random seed for reproducible results
clear all;
close all;
rng(51581489);

%Set up true points
xPoints=[573 773 650 765 465 469 407 250 285 53 67 85 410 432 621 649 625 531 ...
510 479 692 93 397 617 327 341 413 401 518 411 635]';
yPoints=[739 745 643 501 527 473 485 554 469 596 506 411 385 382 475 429 400 ...
372 337 313 319 233 269 206 226 181 161 73 57 23 57]';
len=length(xPoints);

sD=16; %standard deviation

%add my java class
javaaddpath ./ %have to navigate to where I store the code
import exercise2.*
numOfTrials=100; %total number of times to do this
bestDistance=zeros(numOfTrials,1); %vector preallocated (for speed) to store the distances found
for i=1:numOfTrials
    x=xPoints+sD.*randn(len,1);
    y=yPoints+sD.*randn(len,1);
    bestDistance(i)=exercise2.RunSimAnneal.returnDistance(x, y);
end