/**
 * 
 */
package exercise2;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.*;

/**
 * @author end-188do
 * Main Class - runs the whole process
 */
public class RunSimAnneal {
	//Attributes
	private static long[] randomSeeds={51561561616169549L, -9754040499940953L, -2784982188L, 468898385750682977L,
			648885351992766L, 5761308167754839L, -7059277695716694826L, -485375648722841L, 800280468888800L,
			-14188633L, 8627215421761282L, 626275915735525L, 7922073295595L, -959492426392903L, 6557L,35711678574L, 190849129305868777L,
			-3993247757L, 55L, -1678735154857775774L, -7431246920195L, 6554778901755717118L, -66878115L};
			//stores some random seeds that I generated on my calculator.

	/**
	 * Runs the program
	 * @param args : the arguments
	 */
	public static void main(String[] args){

		double[] distances=new double[randomSeeds.length]; //how many iterations to do.
		int bestI=0; //for storing which seed produced the shortest distance
		
		long startTime = System.nanoTime(); //start stopwatch
		for( int i=0; i<randomSeeds.length; i++){ //go through the seeds and run sim anneal for eeach one
			SimAnneal simClass = new SimAnneal(randomSeeds[i]); //insert a random seed.
			simClass.initialisePoints("/Documents/uniwork/4M17/ex2/LocationData.csv");

			simClass.runSimAnneal(); //run it

			distances[i]=RunSimAnneal.convertPixelsToDistance(simClass.getDistance() ); //store all the distances
			if (distances[i]<distances[bestI]) //and store the best one
				bestI=i;
		}
		long endTime = System.nanoTime(); //stop stopwatch
		System.out.println("\n Takes average of "+(endTime - startTime)/randomSeeds.length + " ns"); 
		
		//print out results
		printOutArray(distances, "/Documents/uniwork/4M17/ex2/distances.csv");
		
		//run the best one again with logging enabled (don't do this at first for speed)
		LogClass logger = new LogClass();
		SimAnneal simClass = new SimAnneal(randomSeeds[bestI],logger); //insert a random seed.
		simClass.initialisePoints("/Documents/uniwork/4M17/ex2/LocationData.csv");
		simClass.runSimAnneal();	//runs the sim anneal
		System.out.println("***The best route*** \n" + "Int is " + Integer.toString(bestI) + "\n");
		RunSimAnneal.printOutRoute( simClass);
		System.out.println("\n The distance found was " + Double.toString( distances[bestI] ) + "\n"); 		
		printOutList(logger.getLogPoints(), "/Documents/uniwork/4M17/ex2/currentEversusTemp.csv"); //prints out log.
	}
	
	/**
	 * The following class returns the optimal distance from using the random seeds on the x and y coords in the array input.
	 * This method has been built to complete Task 3 more easily from MATLAB, hence why public static.
	 * Apologies for duplication of code with main. Added this bit after to help with Task 3.
	 * I use this method stright from MATLAB.
	 */
	public static double returnDistance(double[] x, double[] y){
		if (x.length != y.length) //check that the arrays are same size
				throw new IllegalArgumentException();
		
		Route points=new Route(); //set up route using input data
		for (int ii=0; ii<x.length; ii++){ 
			points.addPt(x[ii],y[ii],"loc" + Integer.toString(ii), ii);
		}
		
		
		double[] distances=new double[randomSeeds.length];
		int bestI=0; //for storing which seed produced the shortest distance

		for( int i=0; i<randomSeeds.length; i++){ //loop through random seeds and get distances
			SimAnneal simClass = new SimAnneal(randomSeeds[i]); //insert a random seed.			
			simClass.setPoints(points);
			simClass.runSimAnneal();
			distances[i]=RunSimAnneal.convertPixelsToDistance(simClass.getDistance() ); //store all the distances
			if (distances[i]<distances[bestI]) //and store the best one
				bestI=i;
		}
		return distances[bestI]; //return smallest distance.
		
	}
	
	
	
	/**
	 * Print out array information to a file
	 * @param array : array to print out
	 * @param location : which file to output to
	 */
	static void printOutArray(double[] array, String location){
		String home=System.getProperty("user.home"); //home location
		//note I'm using Unix and I don't think this code will work well with Windows.
		Writer writer = null;

		try {
		    writer = new BufferedWriter(new OutputStreamWriter(
		          new FileOutputStream(home + location), "utf-8"));
		    for(int i=0; i<array.length;i++){ //print out "i, $arrayValue"
		    	writer.write(Integer.toString(i) + "," + Double.toString(array[i]) + "\n");
		    }
		    
		} catch (IOException e) {
		  e.printStackTrace();
		} finally { //now close the file
		   try {
			   writer.close();
		   } catch (Exception e) {
			  e.printStackTrace(); 
		   }
		}
	}
	
	/**
	 * Prints out pair list to file.
	 * Should really build into printOutArray to make code neater. Lots of duplication at the moment.
	 * take advantage of generic programming
	 * @param list : the list to print out
	 * @param location : where to print out.
	 */
	static void printOutList(List<Triplet<Double,Double, Long>> list, String location){	
		String home=System.getProperty("user.home"); //home location - so you cannot see my name in the code!
		//note I'm using Unix and I don't think this code will work well with Windows.
		Writer writer = null;

		try {
		    writer = new BufferedWriter(new OutputStreamWriter(
		          new FileOutputStream(home + location), "utf-8"));
		    for(int i=0; i<list.size();i++){ //print out "i, $arrayValue"
		    	writer.write(Long.toString(list.get(i).z) + "," + Double.toString(list.get(i).x) + "," + Double.toString(list.get(i).y) + "\n");
		    }
		    
		} catch (IOException e) {
		  e.printStackTrace();
		} finally { //now close the file
		   try {
			   writer.close();
		   } catch (Exception e) {
			  e.printStackTrace(); 
		   }
		}
	}
	
	
	/**
	 * Print out route to screen
	 * @param simClass the class to get the route from
	 */
	static void printOutRoute(SimAnneal simClass){
		System.out.println("The route is: \n");
		List<String> list = simClass.getRouteNames();
		for (String s : list)
		{
			System.out.println(s);
		}
	}
	
	/**
	 * Print out distance in m from pixel input to screen
	 * @param simClass
	 */
	private void printOutDistanceFromPixels(SimAnneal simClass){
		System.out.println("\n The distance found was " + Double.toString( RunSimAnneal.convertPixelsToDistance(simClass.getDistance() )) + "\n"); 
	}
	
	
	
	/** 
	 * Converts pixel value to distance in (m).
	 * Note this is linked to the resolution of the screenshot I took from the 
	 * assignment sheet. See my report for further details.
	 * @return distance in m
	 */
	public static double convertPixelsToDistance(double pixels){
		double CON_FACTOR=200.0/134.0037313;
		return pixels*CON_FACTOR;
	}


}
