/**
 * 
 */
package exercise2;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;


/**
 * @author eng-188do
 * This class holds route and runs the simulated annealing code
 */
public class SimAnneal {
	//Constructors

	/**
	 * Constructor 1
	 * @param seed : is the random seed
	 */
	public SimAnneal(long seed){
		SimAnnealBasicConstruct(seed);
		logging=false;
	}

	/**
	 * Constructor 2
	 * @param seed : is the random seed
	 * @param loggerIn : is the log class
	 */
	public SimAnneal(long seed, LogClass loggerIn){
		logging=true;
		logger=loggerIn;
		SimAnnealBasicConstruct(seed);
	}

	/**
	 * This is the basic constructor. It contains all the functions shared by the other constructors
	 * @param seed : the random seed.
	 */
	private void SimAnnealBasicConstruct(long seed){
		points=new Route();
		rnd = new Random(seed);
		iter=0;
	}
	
	//Fields/Properties
	private Route points; //stores the route
	private Random rnd;  // random number generator
	private final Boolean logging; // flag for whether we are logging or not.
	private LogClass logger; // log class
	private long iter; //Stores the iteration number.
						//NOTE: do not currently check that this does not overflow.
	
	//Getters/Setters	
	public void setPoints(Route pointsIn){
		this.points=pointsIn;
	}
	
	public Route getPoints() {
		return points;
	}
	
	
	//Public Methods
	/**
	 * Sets up all the points in memory.
	 */
	public void initialisePoints(String location){
		points.clear(); //clear current route.
		String home=System.getProperty("user.home"); //home dir: so you cannot see my name in the code!
		readInCsv(home + location); //This is where I store the file
	}
	
	/**
	 * Gets the distance of the current route.
	 * @return euclidean distance
	 */
	public double getDistance(){
		return points.getDistance();
	}
	
	/**
	 * 	  @return list of names of points in route in order.
	 * 	  This list is a copy not a reference.
	 */
	public List<String> getRouteNames(){
		List<String> routeNames = new ArrayList<String>(); //where we will store the names
		List<CoordPt> routePoints= new ArrayList<CoordPt>(points.getRoutePts()); //copy route
		
		for(CoordPt point: routePoints){ //copies names across
			routeNames.add(point.getName());
		}

		return routeNames;
	}
	
	/**
	 * Runs simulated Annealing process.
	 */
	public void runSimAnneal(){
		
		double temp=initialTemp(40); //find a good initial temperature
		int maxNumConfig=100*points.getNumPts(); //maximum number of configurations counter
		int maxNumSuccessConfig=50*points.getNumPts(); //maximum number of successful configurations counter
		System.out.println("Initial temp is " + Double.toString(temp));
		
		while (temp > 1e-10) { //run it until the temperature is low enough
			runOneIterSimAnneal(maxNumConfig, maxNumSuccessConfig, temp);
			temp*=0.9; //drop the temp by 10% each time
		}
		
	}
	
	
	//Private Methods
	/**
	 * Runs one iteration of simulated annealing.
	 * By iteration I mean one stage of constant temp.
	 * @param maxNumConfig maximum number of configurations to try
	 * @param maxNumSuccessConfig maximum number of successful configurations to try (ie ones where we have moved)
	 * @param temp temperature
	 */
	private void runOneIterSimAnneal(int maxNumConfig, int maxNumSuccessConfig, double temp) {
		int numConfig=0; //config counter
		int numSuccessConfig=0;		//successful config counter.
		double currentE=points.getDistance(); //current energy (or distance)
		
		while (numConfig <= maxNumConfig && numSuccessConfig <= maxNumSuccessConfig){	//continue until we hit a counter
			Route tempList= new Route(points);	
			
			/* Switching Algorithms uncomment out which to use */
			if (rnd.nextBoolean()){ //two options for choosing new options
				runSplice(tempList); //cut section and paste
			} else{
				runReverse(tempList); //reverse section
			}
			//runSwitch(tempList); //swap two points
			//runNeighbourSwitch(tempList); //swap two neighboring points
			/* End of Switching Algorithms */
			
			double tempE=tempList.getDistance(); //the potential new energy
			double prob=calcProb( tempE, currentE,temp); //calculate probability
			if (rnd.nextDouble() < prob) { //we change at a rate proportional to the probability
				points=tempList;
				currentE=points.getDistance();
				numSuccessConfig++; // we have moved, albeit this could have been uphill.
			}
			if (logging && temp>1) //log only when temp is greater than 1 & we have told the class to.
				performLogging(iter,currentE,temp);
			numConfig++;	//this counter goes up regardless of whether we have moved or not.
			iter++;
		}
	}
	
	/**
	 * Performs the necessary logging
	 * @param iter is the current iteration
	 * @param currentE current energy
	 * @param temp current temperature
	 */
	private void performLogging(long iter, double currentE, double temp) {
		logger.addPoint(currentE,temp, iter);		
	}

	/**
	 * Runs code where we reverse section of string
	 * @param tempList temporary list of the route.
	 */
	private void runReverse(Route tempList) {
		int[] range = generateTwoDistinctInts(tempList.getNumPts());
		tempList.switchPts(range[0], range[1]);		//note changes on the substring affect the main string.
	}
	
	/**
	 * Runs code where we reverse two successive points
	 * @param tempList temporary list of the route.
	 */
	private void runNeighbourSwitch(Route tempList) {
		int numPts=tempList.getNumPts();
		int index = rnd.nextInt(numPts-1)+1; // this format allows us to select all the points (excluding 0 which must be engineering)
		if (index < numPts -2)
			tempList.switchPts(index, index+2); //if not too close to the end swap with the point in front
		else
			tempList.switchPts(index-2, index); // if too close to the end have to swap with the point behind.
		//NOTE: the if form may allow the points at the end to be swapped with higher probability but as our list is quite big
		//it does not have too much of an effect.
	}
	
	
	/**
	 * Runs code where we reverse two non successive points
	 * @param tempList : temporary list of the route.
	 */
	private void runSwitch(Route tempList) {
		int[] range = generateTwoDistinctInts(tempList.getNumPts());
		tempList.swapPts(range[0], range[1]);	//swap the points
	}
	
	/**
	 * Runs code where we remove section of string and paste it elsewhere
	 * @param tempList temporary list of the route.
	 */
	private void runSplice(Route tempList){
		int[] range = generateTwoDistinctInts(tempList.getNumPts()); //calculate the indices to take the section from.
		int numLeft=tempList.getNumPts()-range[1]+range[0];
		
		int insertionPosition;
		if (numLeft==1) //if we have only got 1 item left (ie engineering department at 0) then it must go after.
				insertionPosition=1;
		else //if not then it c an go anywhere after the engineering dept.
			insertionPosition=rnd.nextInt(numLeft-1)+1; //+1 as don't want it to go before 0.

		tempList.removeAndSplice(range[0], range[1], insertionPosition);
	}
	
	
	/** 
	 * Generates two random integers in range of number of points
	 * (excluding 0 as engineering dept must stay there).	 *
	 * @param numPts : number of points in route
	 * @return two integers. they are ordered, cannot be the same or zero.
	 */
	private int[] generateTwoDistinctInts(int numPts) {
		if (numPts <1)
			throw new IllegalArgumentException("Route is too small - solve by hand!");

		//Generate two random numbers between start and end of route
		int[] results = new int[2];
		results[0]=rnd.nextInt(numPts-1)+1; //+1 as don't want it to include 0
		results[1]=rnd.nextInt(numPts-1)+1;
		
		while (results[0]==results[1]) //if results are the same we try again
			results[1]=rnd.nextInt(numPts-1)+1; //get distinct results
		
		Arrays.sort( results );	//order the results
				
		return results;
	}

	/**
	 * Reads in csv of specified file format ie
	 * $name, $xCoord, $yCoord
	 * @param name : where file is located (full path)
	 */
	private void readInCsv(String name){
		BufferedReader br = null;
		String line = ""; //temp variable for holding lines
		String splitter = ","; //what splits our files up
	 
		try {	 
			br = new BufferedReader(new FileReader(name));
			int i=0;
			while ((line = br.readLine()) != null) {
				String[] lineData = line.split(splitter);	//Split up inputs 
				points.addPt(Double.parseDouble(lineData[1]), Double.parseDouble(lineData[2]),
						 lineData[0],i);	 //add point to memory
			}	 
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally { //try closing the file.
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	 
		System.out.println("Read in " + name);
	}
	
	/**
	 * Calulates the probability of moving
	 * @param length1 new potential route's length
	 * @param length2 old potential route's length
	 * @param temp temperature
	 * @return probability (0.0-1.0)
	 */
	private double calcProb(double length1, double length2, double temp) {
		double prob=Math.exp(-(length1-length2)/temp); //sim anneal standard probability
		return (prob>1.0)? 1.0 : prob; //if probability is above 1 round down to 1 to make it make sense.
	}
	
	/**
	 * Calculate the initial temperature and randomly initialise the route.
	 * it calculates the initial temperature to use by looking at the kind of differences you are likely
	 * to encounter. It will also store the best found route in this process as a starting point.
	 * @param trials : number of random initialisations to try
	 * @return temperature
	 */
	private double initialTemp(int trials) {
		double highestE=points.getDistance();
		double lowestE=highestE;
		double tempE; //temporary storage
		int FACTOR=10; //multiply factor at end.
		Route tempList= new Route(points);
		
		//now we look at several other points and see store the highest and lowest energies (ie distances) found
		for(int i=0; i<trials; i++) { 
			tempList.randomiseFull(rnd); //randomise list.
			tempE=tempList.getDistance();
			if (tempE<lowestE){	 //if found a new lowest replace
				lowestE=tempE;
			} else if(tempE>highestE){//if found a new highest replace
				highestE=tempE;
			}			
		}
		points=tempList; // we will also randomly initialise the points to give different starting points
		return FACTOR*(highestE-lowestE);			
	}



}	


