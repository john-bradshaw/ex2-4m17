package exercise2;

import java.util.*;
import org.junit.* ;
import static org.junit.Assert.* ;

/**
 * Class to test the route class
 */
public class TestRoute {
	static double EPSILON_DOUBLE=0.0001;
	
	//Test can add points to route
	@Test
	public void addPtsTest(){
		//initialise
		Route pts=new Route();
		pts.addPt(1, 2, "one", 1);
		pts.addPt(3, 2, "two", 2);
		pts.addPt(5, 7, "three", 3);
		
		
		//run
		List<CoordPt> tester;
		tester=pts.getRoutePts();
		
		//tests
		//for this we will just choose some random points to test
		assertEquals(tester.get(0).getName(),"one");
		assertEquals(tester.get(2).getxVal(),5,EPSILON_DOUBLE);
		assertEquals(tester.get(1).getyVal(),2,EPSILON_DOUBLE);
		assertEquals(tester.get(0).getNumber(),1);		
	}
	
	//Test the distance is correct
	@Test
	public void distanceCheck(){
		//initialise
		Route pts=new Route();
		pts.addPt(1, 2, "one", 1);
		pts.addPt(3, 2, "two", 2);
		pts.addPt(5, 7, "three", 3);
		pts.addPt(2, 3, "four", 4);
		double expectedDistance=12.38516481; //by hand
		double calculatedDistance=0;
		
		//run
		calculatedDistance=pts.getDistance();
		
		//tests
		assertEquals(expectedDistance,calculatedDistance,EPSILON_DOUBLE);
	}
	
	//Test switch points works
	@Test
	public void switchPtsTest1(){
		//initialise
		Route pts=new Route();
		pts.addPt(1, 2, "one", 1);
		pts.addPt(3, 2, "two", 2);
		pts.addPt(5, 7, "three", 3);
		pts.addPt(2, 3, "four", 4);
		pts.addPt(0, 2, "five", 5);
		int start=0;
		int end=2;
		String expectedOne="two";
		String expectedTwo="one";
		String expectedThree="three";
		
		//run
		pts.switchPts(start, end);
		List<CoordPt> tester;
		tester=pts.getRoutePts();
		
		//tests
		//for this we will just choose some random points to test
		assertEquals(tester.get(0).getName(), expectedOne);
		assertEquals(tester.get(1).getName(), expectedTwo);
		assertEquals(tester.get(2).getName(), expectedThree);
		
	}
	
	//Test Switch Points throws correct exceptions
	@Test(expected=IllegalArgumentException.class)
	public void switchPtsTest2() {
		//initialise
		Route pts=new Route();
		pts.addPt(1, 2, "one", 1);
		pts.addPt(3, 2, "two", 2);
		pts.addPt(5, 7, "three", 3);
		pts.addPt(2, 3, "four", 4);
		pts.addPt(0, 2, "five", 5);
		int start=2;
		int end=2;
		String expectedOne="two";
		String expectedTwo="one";
		String expectedThree="three";
		
		//run
		pts.switchPts(start, end);
	}

	//Test Switch Points throws correct exceptions
	@Test(expected=IllegalArgumentException.class)
	public void switchPtsTest3() {
		//initialise
		Route pts=new Route();
		pts.addPt(1, 2, "one", 1);
		pts.addPt(3, 2, "two", 2);
		pts.addPt(5, 7, "three", 3);
		pts.addPt(2, 3, "four", 4);
		pts.addPt(0, 2, "five", 5);
		int start=4;
		int end=2;
		String expectedOne="two";
		String expectedTwo="one";
		String expectedThree="three";
		
		//run
		pts.switchPts(start, end);
	}
	
	//Test remove and splice works
	@Test
	public void removeAndSpliceTest1(){
		//initialise
		Route pts=new Route();
		pts.addPt(1, 2, "one", 1);
		pts.addPt(3, 2, "two", 2);
		pts.addPt(5, 7, "three", 3);
		pts.addPt(2, 3, "four", 4);
		pts.addPt(0, 2, "five", 5);
		int start=1;
		int end=3;
		int insertionPoint = 3; //insert at the end of the smaller list
		String expectedOne="one";
		String expectedTwo="four";
		String expectedThree="five";
		String expectedFour="two";
		String expectedFive="three";
		
		//run
		pts.removeAndSplice(start,end,insertionPoint);
		List<CoordPt> tester;
		tester=pts.getRoutePts();
		
		//tests
		//for this we will just choose some random points to test
		assertEquals(tester.get(0).getName(), expectedOne);
		assertEquals(tester.get(1).getName(), expectedTwo);
		assertEquals(tester.get(2).getName(), expectedThree);
		assertEquals(tester.get(3).getName(), expectedFour);
		assertEquals(tester.get(4).getName(), expectedFive);		
	}
	
	//Test remove and splice works
	@Test
	public void removeAndSpliceTest2(){
		//initialise
		Route pts=new Route();
		pts.addPt(1, 2, "one", 1);
		pts.addPt(3, 2, "two", 2);
		pts.addPt(5, 7, "three", 3);
		pts.addPt(2, 3, "four", 4);
		pts.addPt(0, 2, "five", 5);
		int start=3;
		int end=5;
		int insertionPoint = 0; //insert at the beginning of the smaller list
		String expectedOne="four";
		String expectedTwo="five";
		String expectedThree="one";
		String expectedFour="two";
		String expectedFive="three";
		
		//run
		pts.removeAndSplice(start,end,insertionPoint);
		List<CoordPt> tester;
		tester=pts.getRoutePts();
		
		//tests
		//for this we will just choose some random points to test
		assertEquals(tester.get(0).getName(), expectedOne);
		assertEquals(tester.get(1).getName(), expectedTwo);
		assertEquals(tester.get(2).getName(), expectedThree);
		assertEquals(tester.get(3).getName(), expectedFour);
		assertEquals(tester.get(4).getName(), expectedFive);		
	}
	
}
