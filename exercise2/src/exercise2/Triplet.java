package exercise2;

/**
 * Class to store triplets of values
 */
public class Triplet<X,Y,Z> {
	//Constructor
	public Triplet(X x, Y y, Z z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}

	//Fields/Properties
	public final X x; 
	public final Y y; 
	public final Z z;
}
