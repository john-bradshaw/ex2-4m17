/**
 * 
 */
package exercise2;

/**
 * @author eng-188do
 * This class holds the basic coordinate values for each point.
 */
public class CoordPt {

	//Constructor

	/**
	 * Constructor
	 * @param xIn : x coord
	 * @param yIn : y coord
	 * @param nameIn : name
	 * @param numIn : number id
	 */
	public CoordPt(double xIn, double yIn, String nameIn, int numIn){
		this.xVal=xIn;
		this.yVal=yIn;
		this.name=nameIn;
		this.number=numIn;
	}
	
	//Fields/Properties
	private double xVal, yVal; // x and y coords
	private String name; //name string
	private int number; //number id
	
	
	//Getters/Setters
	public double getxVal() {
		return xVal;
	}
	public double getyVal() {
		return yVal;
	}
	public String getName() {
		return name;
	}
	public int getNumber() {
		return number;
	}
	
	

}
