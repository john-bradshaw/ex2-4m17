package exercise2;

import java.util.*;

import org.junit.* ;

import static org.junit.Assert.* ;

/**
 * Class to test the Simulated annealing class.
 */
public class TestSimAnneal {
	
	private double EPSILON_DOUBLE=0.0001; //how close we want doubles to be.
	
	//Test that we can read in the points correctly
	@Test
	public void testInitialisePoints(){ //check reads inc orrectly by testing a few points
		//initialise
		SimAnneal simClass=new SimAnneal(4563);

		String expectedZero = "Engineering Department";
		String expectedFive= "Pembroke";
		String expectedFifteen="Downing Site Middle";
		String expectedTwentyNine="St John's College";
		String expectedThirty="Christ's College North";
		
		double expectedZeroX=573;
		double expectedZeroY=739;
		double expectedNineteenX=479;
		double expectedTwentyTwoY=269;
	
		
		//run
		simClass.initialisePoints("/Documents/uniwork/4M17/ex2/LocationData.csv");
		List<String> tester;
		tester=simClass.getRouteNames();
		Route setPts =	simClass.getPoints();
		List<CoordPt> points= setPts.getRoutePts();
		
		//tests
		//for this we will just choose some random points to test
		//test some names
		assertEquals(tester.get(0),expectedZero);
		assertEquals(tester.get(5),expectedFive);
		assertEquals(tester.get(15),expectedFifteen);
		assertEquals(tester.get(29),expectedTwentyNine);	
		assertEquals(tester.get(30),expectedThirty);
		
		//test some coords
		assertEquals(expectedZeroX,points.get(0).getxVal(),this.EPSILON_DOUBLE);
		assertEquals(expectedZeroY,points.get(0).getyVal(),this.EPSILON_DOUBLE);
		assertEquals(expectedNineteenX,points.get(19).getxVal(),this.EPSILON_DOUBLE);
		assertEquals(expectedTwentyTwoY,points.get(22).getyVal(),this.EPSILON_DOUBLE);
	
	}
	
	
	//Test that first lcoation remains fixed when running Sim. Anneal.
	@Test
	public void simAnnealtest(){
		//initialise
		Route pts=new Route();
		pts.addPt(0, 0, "one", 1);
		pts.addPt(3, 2, "two", 2);
		pts.addPt(2, 2, "three", 3);
		pts.addPt(1, 1, "four", 4);
		pts.addPt(0, 1, "five", 5);
		SimAnneal simClass=new SimAnneal(151461465);
		simClass.setPoints(pts); // put route into class
		double expectedInitialDistance=7.019764838; //on calculator
		double expectedOptimisedRoute=4.414213562; //simple problem visually
		double actualInitialDistance=0;
		double actualEndDistance=0;
		String expectedZero = "one";
		
		//run
		actualInitialDistance=simClass.getDistance();
		simClass.runSimAnneal();
		actualEndDistance=simClass.getDistance();
		List<String> tester;
		tester=simClass.getRouteNames();
		
		//tests
		//for this we will just choose some random points to test
		assertEquals(tester.get(0),expectedZero); //check one stays at the beginning
		assertEquals(expectedInitialDistance,actualInitialDistance,this.EPSILON_DOUBLE);
		/*Note I removed the last test (below) as sometimes it would fail, as the simulated annealing algorithm
		 * isn't guaranteed to find the minimum, even for simple case such as this. 
		 */
		//assertEquals(expectedOptimisedRoute,actualEndDistance,this.EPSILON_DOUBLE);
		

	}
}
