/**
 * 
 */
package exercise2;

import java.util.*;



/**
 * @author eng-188do
 * This holds a series of coordinate points that make up the route.
 */
public class Route {
	//Constructors

	/**
	 * Constructor
	 */
	public Route(){
		routePts = new ArrayList<CoordPt>();
	}

	/**
	 * Copy constructor
	 * @param another : the Route to copy.
	 */
	public Route(Route another) { //Copy constructor
		    this.routePts = new ArrayList<CoordPt>(another.routePts); 
	 }
	
	//Fields/Properties
	private List<CoordPt> routePts; //stores the route points
	

	// Getters/Setters
    public List<CoordPt> getRoutePts() {
        return this.routePts;
    }
	
	
	//Public Methods
	/**
	 * Randomises the route by randomly swapping the locations
	 */
	public void randomiseFull(Random rnd){
		List<CoordPt> sub = routePts.subList(1, routePts.size()); // don't want to shuffle first one, as it must stay engineering dept.
		Collections.shuffle(sub, rnd);
	}
	
	
	/**
	 * Randomly switch an atom of the points so those in route between indices go backwards
	 * @param start start point to switch
	 * @param end end point to switch
	 */
	public void switchPts(int start, int end){
		checkLegality( start,  end);
		
		List<CoordPt> sub = routePts.subList(start, end); //create sublist
		Collections.reverse(sub);
	}
	
	/**
	 * @return euclidean distance of the route
	 */
	public double getDistance(){
		double distance=0;
		for(int i=1; i<this.getNumPts(); i++){ //go through the points and add the distance between each point.
			double distX=routePts.get(i).getxVal()-routePts.get(i-1).getxVal(); // x distance between each point
			double distY=routePts.get(i).getyVal()-routePts.get(i-1).getyVal(); // y distance
			double sum=Math.pow(distX, 2)+Math.pow(distY, 2); //euclidean distance
			distance+=Math.sqrt(sum); //add the distance between two points to the total distance
		}
		return distance;
	}
	
	/**
	 * @return number of points in the route
	 */
	public int getNumPts(){
		return routePts.size();
	}

	/**
	 * Adds a point to the end of the route
	 * @param xVal : x value
	 * @param yVal : y value
	 * @param name : name
	 * @param num : id number
	 */
	public void addPt(double xVal, double yVal, String name, int num){
		CoordPt newPt= new CoordPt(xVal, yVal, name, num);
		routePts.add(newPt);
	}
	
	
	/**
	 * Clears the route ie removes all points
	 */
	public void clear(){
		routePts.clear();
	}

	/**
	 * Moves the sublist index by start and end to the new position
	 * @param start start point of sublist
	 * @param end end point of sublist
	 * @param insertionPosition where to put sublist in smaller list
	 */
	public void removeAndSplice(int start, int end, int insertionPosition){
		checkLegality( start,  end); //checks index legality
		List<CoordPt> sub = new ArrayList<CoordPt>(routePts.subList(start, end)); //copy sublist
		routePts.removeAll(sub); //remove the sublist from the main list
		
		if(insertionPosition<0 || insertionPosition> routePts.size())
			throw new IllegalArgumentException("insertion point does not fit in smaller list");
		
		routePts.addAll(insertionPosition,sub); //add the sublist back into the main list but in its new position.
	}

	/**
	 * Swaps two points with specified indices.
	 * @param i first index
	 * @param j second index
	 */
	public void swapPts(int i, int j) {
		Collections.swap(routePts,  i,  j);		
	}



	//Private Members

	/**
	 * Checks the legality of the indices for splicing or swapping
	 * ie they must be in order and in range of the list.
	 * @param start : start index
	 * @param end : end index
	 */
	void checkLegality(int start, int end){
		if (end <= start)
			throw new IllegalArgumentException("switch points must have two points in order and at least distance one apart");

		//check that it is in range whilst debugging:
		assert start>-1: "Start is less than 0!";
		assert end<=routePts.size(): "End is longer than the length of the route!";
	}
	
}
