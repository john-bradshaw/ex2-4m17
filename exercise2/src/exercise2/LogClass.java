/**
 * 
 */
package exercise2;

import java.util.*;

/**
 * @author eng-188do
 * Class to store logpoints
 */
public class LogClass {
	//Constructor
	LogClass(){
		logPoints = new ArrayList<Triplet<Double,Double, Long>>();
	}
	
	//Fields/Properties
	private List<Triplet<Double,Double, Long>> logPoints; //stores the log points
	
	//getters/setters
	public List<Triplet<Double,Double, Long>> getLogPoints() {
		return logPoints;
	}
	
	//Public Methods

	/**
	 * Adds a point to the log
	 * @param currentE : the distance (or energy) to add
	 * @param temp : the current temperature
	 * @param iter : the iteration number
	 */
	public void addPoint(double currentE, double temp, long iter) {
		logPoints.add(new Triplet<Double, Double, Long>(currentE,temp, iter));
	}
	

}
